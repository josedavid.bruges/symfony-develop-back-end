<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

/**
 * @Route("/api")
 */
class AuthController extends AbstractController
{
    private $userRepository;
    private $JWTManager;
    private $security;
    
    public function __construct(
        UserRepository $userRepository,
        Security $security,
        JWTTokenManagerInterface $JWTManager
    ) {
        $this->userRepository = $userRepository;
        $this->JWTManager = $JWTManager;
        $this->security = $security;
    }

    /**
     * @Route("/auth", name="app_auth")
     */
    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
        ]);
    }

    /**
     * @Route("/loginAuth", name="loginAuth")
     */
    public function loginAuth(Request $request): Response
    {
        $jsonData = json_decode($request->getContent());
        $userResponse = $this->userRepository->findOneByCredentials(Array(
            "email" => $jsonData->email,
            "password" => $jsonData->password
        ));
        if(!$userResponse){
            return new JsonResponse([
                'status' => 'fail',
                'message' => 'User not found',
                'token' => null
            ], 500);
        }
        if($userResponse['status'] != "success"){
            return new JsonResponse([
                'status' => $userResponse['status'],
                'message' => $userResponse['message'],
                'token' => null
            ], 500);
        }
        $token = "";
        try {
            $tokenResponse = $this->JWTManager->create($userResponse['data']);
            $token = $tokenResponse;
        } catch (\Throwable $th) {
            return new JsonResponse([
                'status' => 'failGenerateToken',
                'message' => 'Token not found',
                'token' => null
            ], 500);
        }

        return new JsonResponse([
            'status' => 'success',
            'message' => 'Create Token Auth',
            'token' => $token,
            'userAuth' => Array(
                "id" => $userResponse['data']->getId(),
                "email" => $userResponse['data']->getEmail(),
                "parentId" => $userResponse['data']->getParentId(),
            )
        ], 200);
    }

    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request): Response
    {
        $jsonData = json_decode($request->getContent());

        $filter = $this->userRepository->findOneByEmail($jsonData->email);
        if ( $filter ) 
        {
            return new JsonResponse([
                'status' => 'duplicateEmail',
                'message' => 'Duplicate email.',
                'data' => $jsonData->email
            ], 500);
        } else {
            $response = $this->userRepository->createUser($jsonData);
            return new JsonResponse([
                'status' => 'success',
                'message' => 'Registered user',
                'data' => $response
            ], 200);
        }
    }

    /**
     * @Route("/getListByParantId", name="getListByParantId")
     */
    public function getListByParantId(Request $request): Response
    {
        $username = "";
        try {
            $token = $request->headers->get('Authorization');

            $tokenParts = explode(".", $token);
            if(count($tokenParts) < 1){
                return new JsonResponse([
                    'status' => 'invalidToken',
                    'message' => 'Token not found',
                    'token' => null
                ], 500);
            }
            $tokenPayload = base64_decode($tokenParts[1]);
            $jwtPayload = (Array)json_decode($tokenPayload);

            $currentDate = time() * 1000;

            if($currentDate > $jwtPayload['exp'] * 1000){
                return new JsonResponse([
                    'status' => 'expiredToken',
                    'message' => 'Expired token',
                    'token' => null,
                    'iat' => $jwtPayload['iat'] * 1000,
                    'exp' => $jwtPayload['exp'] * 1000,
                    'system' => $currentDate,
                    'iatFormat' => date("Y-m-d", $jwtPayload['iat']),
                    'expFormat' => date("Y-m-d", $jwtPayload['exp']),
                    'systemFormat' => date("Y-m-d", time())
                ], 500);
            }

            $username = $jwtPayload["username"];
            if ( $username == null )
            {
                return new JsonResponse([
                    'status' => 'failVerifyToken',
                    'message' => 'Verify Token error (-1)',
                    'token' => null
                ], 500);
            }
            if ( strlen($username) == 0 )
            {
                return new JsonResponse([
                    'status' => 'failVerifyToken',
                    'message' => 'Verify Token error (-2)',
                    'token' => null
                ], 500);
            }

        } catch (\Throwable $th) {
            return new JsonResponse([
                'status' => 'failGenerateToken',
                'message' => 'Token not found',
                'token' => null
            ], 500);
        }
        $userParent = $this->userRepository->findOneByEmail($username);

        $filter = $this->userRepository->getListByParantId($userParent->getId());
        if ( $filter ) 
        {
            return new JsonResponse([
                'status' => 'success',
                'message' => 'User list',
                'data' => $filter
            ], 200);
        } else {
            return new JsonResponse([
                'status' => 'emptyList',
                'message' => 'Empty user list',
            ], 500);
        }
    }
}
