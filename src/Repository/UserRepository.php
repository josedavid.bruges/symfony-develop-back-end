<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use PhpParser\Node\Expr\Cast\Object_;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;



/**
 * @extends ServiceEntityRepository<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    private $passwordEncoder;
    public function __construct(ManagerRegistry $registry, UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct($registry, User::class);
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(User $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(User $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function createUser($data)
    {
        $user = new User();
        $user->setEmail($data->email);
        $hash = password_hash($data->password, PASSWORD_BCRYPT, ['cost' => 13]);
        $user->setPassword($hash);
        $user->setParentId($data->parentId);

        $this->_em->persist($user);
        $this->_em->flush();

        return Array(
            "id" => $user->getId(),
            "parentId" => $user->getParentId(),
            "email" => $user->getEmail()
        );
    }

    public function findOneById($id): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.id = :val')
            ->setParameter('val', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
    public function findOneByEmail($email): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.email = :val')
            ->setParameter('val', $email)
            ->getQuery()
            ->getOneOrNullResult();
    }
    public function findOneByCredentials($credentials): ?Array
    {
        $response = $this->createQueryBuilder('u')
            ->andWhere('u.email = :email')
            ->setParameter('email', $credentials['email'])
            ->getQuery()
            ->getOneOrNullResult();
        if ( !$response ) 
        {
            return Array(
                "status" => "EmailNotFound",
                "message" => "The email address does not exist"
            );
        }
        if (password_verify($credentials['password'], $response->getPassword())) {
            $user = new User();
            $user->setId($response->getId());
            $user->setEmail($response->getEmail());
            $user->setPassword($response->getPassword());
            $user->setParentId($response->getParentId());
            return Array(
                "status" => "success",
                "message" => "Correct credentials",
                "data" => $user
            );
        } else {
            return Array(
                "status" => "wrongPassword",
                "message" => "You have entered a wrong password"
            );
        }
    }
    public function getListByParantId($parentId): ? Array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT
                p.id,
                p.parentId,
                p.email
            FROM App\Entity\User p
            WHERE p.parentId = :parentId
            ORDER BY p.id ASC'
        )->setParameter('parentId', $parentId);

        // returns an array of Product objects
        return $query->getResult();
    }

}
